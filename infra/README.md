***#Infra***
-------------

Instruções iniciais:
--------------------

Para utilizar este ambiente é necessário antes ter instalado o docker-engine no caso *nix ou docker-toolbox caso MAC ou Windows.

Instruções de instalação :
--------------------------------

*nix :-> https://docs.docker.com/engine/installation/linux/ubuntulinux/

Mac ou Windows :-> https://www.docker.com/products/docker-toolbox

*Aqui tem um ótimo passo-a-passo -> https://youtu.be/0BmWDysy9RY*

Precisaremos também do docker-compose, que pode ser instalado seguindo este  guia: 

https://docs.docker.com/compose/install/

Para verificar se a instalação está rodando corretamente, no terminal do linux ou no terminal do docker-toolbox (sem sudo):

Teste1
```
sudo docker run hello-world
```
Teste 2
```
sudo docker-compose --version
```


----------


***Agora vamos à prática:***

Ambiente inicial, release 0.2

Vamos rodar o ambiente com Docker-Compose, que é um auxiliar, que irá levantar um ambiente completo, como um script que iniciar várias máquinas ao mesmo tempo, porém nele podemos adicionar parâmetros, como path da aplicação, senha do mysql etc, e ainda podemos escolher uma parte ou todo o projeto para inciar.

Para rodar o projeto completo:
1 - Vá ao diretório onde clonou o projeto e dentro do diretório "inicial", rode o comando a seguir (apenas no caso *nix usar sudo):

``` 
sudo docker-compose up -d
``` 

Na primeira vez ele irá baixar os arquivos necessários, mas nas próximas vezes tudo estará em seu computador, então você deverá receber uma saída similar a isso:
``` 
Starting inicial_mysql_1
Starting inicial_front_1
``` 

E então é só isso, o ambiente inicial ( php + mysql) está no ar, 2 containers distintos se comunicando, para acessá-los em seu navegador digite:

*Linux:*

localhost

*Mac+Windows:*

Ip informado no terminal do docker-toolbox que normalmente é *192.168.99.100*


Agora, caso deseje levantar apenas um dos containers, basta informar no comando do docker compose, por exemplo, se quiser apenas o mysql:

``` 
sudo docker-compose up mysql -d
``` 

Assim, abstraímos a parte de build e de run anteriores, e adicionamos camadas de link de rede entre os containers, tudo isso por meio de um arquivo simples de YAML, facilmente alterado por qualquer um para uma necessidade específica. 

Para "desligar" as máquinas, você pode executar:

``` 
sudo docker-compose down
``` 

Adicionamos o NoSQL DynamoDB da AWS ao ambiente, que pode ser acessado diretamente pelo nome, ou pela porta, como no teste simples abaixo:

``` 
aws dynamodb list-tables --endpoint-url http://localhost:8000
``` 

Para utilizar o CLI da AWS, siga este link ->  http://aws.amazon.com/cli/